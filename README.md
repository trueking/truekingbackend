## Install

- Install a postgresql with trueking user that has trueking password

## Heroku Link

- https://dry-plateau-50413.herokuapp.com/

## API REST schema
```text
Public:
/accounts/
```
Method|Path|Required Data|Return|Effect
---|---|---|---|---
GET|/existEmail/{email}|url->email|Boolean|returns true or false if email is in the db
GET|/existUserName/{userName}|url->userName|Boolean|returns true or false if there's a user with login = userName in the db
POST|.|Body->UserData|UserDataWithNoPassword|creates a new user
POST|/login|Body->Token|String|returns "true" if login succesfull otherwise returns "false"
PUT|/resetPassword|Body encripted->email|Boolean|resets the password of the user that have the email and send the new password to the user by mail. Returns false if the mail is not valid.
```text
UserData structure:
{
    credentials:
    {
        userName: String
        password: String
    }
    name: String
    surnames: String
    email: String
    zipCode: String
    porfilePicture: String
    wishedTags: List<String>
}

UserDataWithNoPassword structure:
{
    credentials:
    {
        userName: String
    }
    id: Integer
    name: String
    surnames: String
    email: String
    zipCode: String
    profilePicture: String
    wishedTags: List<String>
}
```

```text
Secured:
/profiles/
```
Method|Path|Required Data|Return|Effect
---|---|---|---|---
GET|.| |List<UserDataWithNoPassword>|returns array of all users in the db
GET|/mine| |UserDataWithNoPassword|returns the user own info
GET|/mine/wishlist| |ListTags|returns the user own wish list
GET|/{userid}|url->userid|UserDataWithNoPassword|returns the public info of the user that have id = userid
GET|/{userid}/offers|url->userid|List<ItemFrontendWithHistoric>|returns all active items of the userid
PUT|/mine|Body->UserDataWithNoPassword|UserDataWithNoPassword|updates the user own info
PUT|/mine/wishlist|Body->ListTags|ListTags|updates the own user wish list
PUT|/mine/changePassword|Body encripted->newPassword||Change the password of the user
DELETE|/mine| | |Deletes the account and their associated items
DELETE|/{userid}/offers/{itemidref}|url->userid, url->itemidref| |deletes the itemidref of the user userid
DELETE|/{userid}/wishlist/{tagname}|url->userid, Body->tagname| |delete the tag = tagname to the userid wishlist

```text
ListTags structure:
{
    wishedTags: List<String>
}

ItemFrontendWithHistoric structure:
{
    id: Integer
    name: String
    description: String
    dirty: Boolean
    owner: Integer
    historic: Boolean
    imgs: List<String>
    tags: List<String>
    wishedTags: List<String>
}
```

```text
Secured:
/items/
```
Method|Path|Required Data|Return|Effect
---|---|---|---|---
GET|.| |List<ItemFrontend>|returns array of items
GET|/myItems| |List<ItemFrontendWithHistoric>|returns the own items of the user
GET|/search|Query Param->tags|List<ItemFrontend>|return the items that have the requested tags and those items are not from the user that makes the call
GET|/search/{itemname}|url->itemname|List<ItemFrontend>|returns the items wich the name contains itemname and the items are not from the own user
GET|/search/zipCode/{itemname}|url->itemname|List<ItemFrontend>|returns the items wich the name contains itemname and the items are not from the own user and the zipCode of the owner of the item is the same as the zipCode of the user
GET|/search/somethingIwish| |List<ItemFrontend>|returns all items that have atleast one tag wich is in the user wishlist
GET|/search/byOwnersWishList| |List<ItemFrontend>|returns all items that the user of the item wants somet item that the user of the call is offering
GET|/{itemid}|url->itemid|ItemFrontend|returns the itemid
GET|/autocomplete/{itemname}|url->itemname|List<ItemFrontend>|returns the items that start with itemname
GET|/{itemid}/images|url->itemid|List<ImageItemDTO>|returns the images of the itemid
GET|/{itemid}/images/{imageid}|url->itemid, url->imageid|ImageItemDTO|returns the iamge = imageid form the item = itemid
GET|/{itemid}/tags|url->itemid|List<TagDTO>|returns tags attached to the item
GET|/{itemid}/tags/{tagname}|url->itemid, url->tagname|TagDTO|returns the tagname attached to the itemid
POST|.|Body->ItemFrontend|ItemFrontend|insert an item item with owner from the identity of the caller
POST|/{itemid}/images|url->itemid, Body->ImageItemDTO|List<ImageItemDTO>|add the image to the item = itemid
POST|/{itemid}/tags|url->itemid, Body->TagDTO|List<TagDTO>|attaches a tag to the item (posting it to the system if not exists)
PUT|/{itemid}|url->itemid, Body->ItemFrontend|ItemFrontend|update an item item with owner from the identity of the caller
DELETE|/{itemid}/tags/{tagname}|url->itemid, url->tagname| |detaches tag of the item
DELETE|/{itemid}/images/{imageid}|url->itemid, url->imageid| |deletes the imageid from the itemid
DELETE|/{itemid}|url->itemid| |deactivate item
```text
ItemFrontend structure:
{
    id: Integer
    name: String
    description: String
    dirty: Boolean
    owner: Integer
    imgs: List<String>
    tags: List<String>
    wishedTags: List<String>
}

Query Param->tags structure:
{
    tags: List<String>
}

ImageItemDTO structure:
{
    id: Integer
    img: String
}

TagDTO structure:
{
    id: Integer
    name: String
}
```
```text
Secured:
/tags/
```
Method|Path|Required Data|Return|Effect
---|---|---|---|---
GET|.| |List<TagDTO>|return all the tags in the db
GET|/{tagname}|url->tagname|TagDTO|returns the tag with name = tagname
PUT|/{tagname}|url->tagname, Body->tag|TagDTO|updates the name of the tag with tagname
POST|.|Body->tag|TagDTO|stores the tag in the db
DELETE|/{tagname}|url->tagname| |removes the tag with the name = tagname
GET|/autocomplete/{tagname}|url->tagname|List<String>|returns all the tags that starts with tagname
GET|/{tagname}/references|url->tagname|List<TagDTO>|returns all the references of the tagname
GET|/{tagname}/references/{tagnameref}|url->tagname, url->tagnameref|TagDTO|returns teh tag tagnameref of the tag tagname
POST|/{tagname:.+}/references|url->tagname, Body->tagref|List<TagDTO>|add the tagref to the references of the tag = tagname
DELETE|/{tagname}/references/{tagnameref}|url->tagname, url->tagnameref| |deletes the tagnameref from the references of the tagname

```text
Secured:
/chats/
```
Method|Path|Required Data|Return|Effect
---|---|---|---|---
GET|.| |List<ChatDTO>|returns array of all chats
GET|/mychats| |List<ChatDTO>|returns array of chats user participates in
GET|/{chatid}|url->chatid|ChatDTO|returns one chat (user must be a participant)
DELETE|/{chatid}|url->chatid|Boolan|deletes chat (user must be a participant). Provisional
POST|/{chatid}|url->chatid, body->ChatMessageDTO|ChatMessageDTO|posts a message to a chat (user must be a participant)
DELETE|/{chatid}/{messageid}|url->chatid, url->messageid| |deletes the messageid from the chatid
```text
ChatDTO structure:
{
    id: Integer
    participants: List<UserDTO>
    truekGroup: TruekingGroup
    messages: List<ChatMessageDTO>
    active: Boolean
}
ChatMessageDTO structure:
{
    id: Integer
    sender: String
    message: String
    active: Boolean
}
```
```text
Secured:
/trueques/
```
Method|Path|Required Data|Return|Effect
---|---|---|---|---
GET|.| |List<TruekingGroupAPI>|returns array of arrays of TruekingGroups
POST|.|Body->TruekingAPI|TruekingGroup|returns identifier of the TruekingGroup
GET|/{truequeid}|url->truequeid|List<TruekingAPI>|returns array of my Truekings from this TruekingGroup
GET|/{truequeid}/{exchangeid}|url->truequeid, url->exchangeid|TruekingAPI|returns the info of my Trueking
PUT|/{truequeid}/{exchangeid}|url->truequeid, url->exchangeid|TruekingAPI|returns the info of my Trueking

```text
Trueking structure:
{
id: Integer,
active: boolean,
accepted: Boolean,
selfPart: TruequePart,
otherPart: TruequePart,
}

TruequePart structure:
{
usedID: integer,
acceance: Boolean,
items: [ItemDTO]
}

TruekingGroup structure:
{
id: integer,
accepted: Boolean,
active: Boolean
}
```

## Basic Structure

```text
project/
|
|-- com.trueking.backend/               # Executable package, it only has the executable class
|
|-- com.trueking.backend.config/        # Configuration package. DON'T TOUCH ANYTHING FROM HERE
|
|-- com.trueking.backend.controller/    # Controllers package. It contains the REST mapping classes. Classes MUST has @RestController anotation
|
|-- com.trueking.backend.exceptions/    # Exceptions package
|
|-- com.trueking.backend.model/         # Models package. They has to start with @Entity anotation
|
|-- com.trueking.backend.repository/    # Repositories package. They're from JPA. Need to start with @Repositry anotation
|
|-- com.trueking.backend.security/      # Security package
|
|-- com.trueking.backend.server/        # Another configuration package, don't touch!
|
|-- com.trueking.backend.service/       # Service package
|
|-- com.trueking.backend.service.impl/  # Service Implementations package
```

Remember to create a new branch from develop any time you want to start a new US.

## Useful links
http://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.repositories