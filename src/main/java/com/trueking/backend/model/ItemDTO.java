package com.trueking.backend.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Item")
public class ItemDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;

	@Column(name = "nameItem")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "idOwner")
	private Integer owner;

	/*@OneToMany
	@JoinColumn(name = "id", referencedColumnName = "id")
	private List<ItemImageDTO> imgs;

	@ManyToMany
	@JoinTable(name = "ItemxTag",
		joinColumns = @JoinColumn(name = "idItem", referencedColumnName = "id"),
		inverseJoinColumns = @JoinColumn(name = "idTag", referencedColumnName = "id"))
	private List<TagDTO> tags;*/

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getOwner() {
		return owner;
	}

	public void setOwner(Integer owner) {
		this.owner = owner;
	}

	/*public List<ItemImageDTO> getImgs() {
		return imgs;
	}

	public void setImgs(List<ItemImageDTO> imgs) {
		this.imgs = imgs;
	}

	public List<TagDTO> getTags() {
		return tags;
	}

	public void setTags(List<TagDTO> tags) {
		this.tags = tags;
	}
	
	public void addTag(TagDTO tag){
		if(!this.hasTag(tag)){
			tags.add(tag);
		}
	}
	
	public void removeTag(TagDTO tag){
		if(this.hasTag(tag)){
			tags.remove(tag);
		}
	}
	
	public Boolean hasTag(TagDTO tag){
		Boolean answer = false;
		Integer i = 0;
		while(!answer && i<tags.size()){
			if(tags.get(i).getName().equals(tag.getName())){
				answer = true;
			}
			++i;
		}
		return answer;
	}
	
	public void addImage(ItemImageDTO img){
		if(!this.hasImg(img)){
			imgs.add(img);
		}
	}
	
	public void removeImg(ItemImageDTO img){
		if(this.hasImg(img)){
			imgs.remove(img);
		}
	}
	
	public Boolean hasImg(ItemImageDTO img){
		Boolean answer = false;
		Integer i = 0;
		while(!answer && i<imgs.size()){
			if(imgs.get(i).getImg().equals(img.getImg())){
				answer = true;
			}
			++i;
		}
		return answer;
	}*/
}
