package com.trueking.backend.controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;
import com.trueking.backend.model.ItemDTO;
import com.trueking.backend.repository.ItemDTORepository;

@RestController
@RequestMapping(value = "/items", produces = MediaType.APPLICATION_JSON_VALUE)
public class ItemDTOController {
//TODO: informar-se sobre ResponseEntity
    @Inject
    ItemDTORepository itemRepository;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ItemDTO> getItems(){
    	System.out.println("getItems()");
    	List<ItemDTO> itemsList = Lists.newArrayList(itemRepository.findAll());
        return itemsList;
    }

    @RequestMapping(value = "/{itemid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ItemDTO getOne(@PathVariable Integer itemid) {
        return itemRepository.findOne(itemid);
    }
    
    @RequestMapping(value = "/{itemid}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ItemDTO updateOne(@RequestBody ItemDTO item) {
        return itemRepository.save(item);
    }
    
    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ItemDTO insertItem(@RequestBody ItemDTO item){
    	return itemRepository.save(item);
    }
    
    @RequestMapping(value = "/{itemid}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteOne(@PathVariable Integer itemid) {
        itemRepository.delete(itemid);
    }


}
