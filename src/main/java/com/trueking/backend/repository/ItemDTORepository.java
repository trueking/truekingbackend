package com.trueking.backend.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.trueking.backend.model.ItemDTO;

@Repository
public interface ItemDTORepository extends CrudRepository<ItemDTO,Integer>{
}
